import logging
import jwt
import time
import sys

from aiohttp import web
from api.v1.constants import APP_DB_MANAGER

shared_key = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsuSfXS5pRgzQRDf/3oyu
H2jdpM7aVgTBZxRXlZdt5A5Ossxeg7oI/Vsv2Cb+2cTR7GD+O7mbKceYgxUejIAU
YBwyIZt7Y5bKN49CB2mUBgkO74eZzfFR0rU6HWoK4WZOkvvfDXlGMwvQ972LTFoe
ly8yVHQ+r5Fkb/aJF9yNZOrnbuqA18kA+lEJYtGzt/N71roDfSgveKnxEv+2vOOc
C/TdK86NhSXzk/5LSjRRQyUxW9DGvdWx+zQ6HdqAwYL9eWmlQmT3ojq6+SbVI2Ux
M8pVeAARUdy06bLVS6HtfFTaeOpS+l0L7eWbmaziJsZCgsMdSIU/j4uwmgwNsatm
YwIDAQAB
-----END PUBLIC KEY-----"""


async def authorize_request(request: web.Request, token_in_url=False):
    global shared_key

    if token_in_url:
        token = str(request.query["token"])
    else:
        token = request.headers.get('authorization')
    logging.info("authorization_request(): token: " + str(token))

    if token is None:
        raise Exception("token is not found")

    token = token.replace('Bearer', '').strip()

    if token is "":
        raise Exception("token is empty")

    try:
        data = jwt.decode(token, key=shared_key, algorithms=['RS256'], verify=False)

    except jwt.InvalidTokenError:
        raise Exception("decode token error")

    expire_at = data['exp']

    if time.time() > expire_at:
        raise Exception("token is expired")

    try:

        query = ("select token from tokens where user = '%s'", (data['username']))

        client = request.app[APP_DB_MANAGER]

        await client.fetch_all(query)

    except Exception as ex:
        logging.error("authorization_request(): get token from db error: " + str(sys.exc_info()))
        raise Exception("get token from database error")
