"""
routes
"""

from api.v1.handlers.authorize import *


def setup_routes(app, provider, handler):
    app.router.add_post(provider.token_path, handler.post_dispatch_request)
    app.add_routes([
        web.post('/oauth/token', authorize),
        web.get('/echo', get_echo)])
