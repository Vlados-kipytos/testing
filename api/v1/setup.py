from aiohttp import web

from api.v1.constants import *
from .routes import setup_routes


def setup_api_v1(app: web.Application, provider, handler) -> None:
    subapp = web.Application()

    subapp[APP_DB_MANAGER] = app[APP_DB_MANAGER]

    setup_routes(subapp, provider, handler)

    app.add_subapp('/api/v1/', subapp)
