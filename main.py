import asyncio

from aiohttp import web
from oauth2 import Provider
from oauth2.grant import ResourceOwnerGrant, RefreshToken
from oauth2.store.memory import ClientStore, TokenStore
from oauth2.tokengenerator import Uuid4TokenGenerator
from oauth2.web import ResourceOwnerGrantSiteAdapter
from oauth2.web.aiohttp import OAuth2Handler
from service.database.client import Manager as MCUDatabaseManager
from api.v1.setup import *

from api.v1.constants import APP_DB_MANAGER
from api.v1.routes import setup_routes


class TestSiteAdapterOwner(ResourceOwnerGrantSiteAdapter):
    def authenticate(self, request, environ, scopes, client):
        example_user_id = 123
        example_ext_data = {}
        username = request.post_param("username")
        password = request.post_param("password")

        if username == "admin" and password == "admin":
            return example_ext_data, example_user_id


async def init_app() -> web.Application:
    app = web.Application()

    app[APP_DB_MANAGER] = MCUDatabaseManager(
        host='127.0.0.1',
        port=5432,
        user='postgres',
        password='',
        database='postgres'
    )

    setup_api_v1(app, provider, handler)
    return app




def main():
    client_store = ClientStore()
    client_store.add_client(client_id="mcu", client_secret="abc", redirect_uris=["http://localhost:8080/callback"])
    token_store = TokenStore()
    global provider
    provider = Provider(access_token_store=token_store,
                        auth_code_store=token_store, client_store=client_store,
                        token_generator=Uuid4TokenGenerator())
    provider.add_grant(ResourceOwnerGrant(site_adapter=TestSiteAdapterOwner(), expires_in=1000))
    provider.add_grant(RefreshToken(expires_in=15768000))
    global handler

    handler = OAuth2Handler(provider)

    app = web.Application()

    setup_routes(app, provider, handler)
    web.run_app(app)


if __name__ == '__main__':
    main()
